package com.atlassian.plugin.predicate;

import com.atlassian.plugin.ModuleDescriptor;

import java.util.function.Predicate;

/**
 * A {@link Predicate} that matches enabled modules.
 */
public class EnabledModulePredicate implements Predicate<ModuleDescriptor<?>> {
    public EnabledModulePredicate() {
    }

    public boolean test(final ModuleDescriptor<?> moduleDescriptor) {
        return moduleDescriptor.isEnabled() && !moduleDescriptor.isBroken();
    }
}

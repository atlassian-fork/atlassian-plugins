package com.atlassian.plugin.event.listeners;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PassListener {
    private final Class<?> clazz;
    private int called = 0;

    public PassListener(final Class<?> clazz) {
        this.clazz = clazz;
    }

    public void channel(final Object o) {
        if (clazz.isInstance(o)) {
            called++;
        }
    }

    public void assertCalled() {
        assertTrue("Event not thrown " + clazz.getName(), called > 0);
        reset();
    }

    public void assertCalled(final int times) {
        assertEquals("Event not thrown " + clazz.getName(), times, called);
        reset();
    }

    public void reset() {
        called = 0;
    }
}

package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;

/**
 * Event that signifies the plugin framework has been shutdown.
 *
 * @see com.atlassian.plugin.event.events
 * @since 2.0.0
 */
@PublicApi
public class PluginFrameworkShutdownEvent extends PluginFrameworkEvent {
    public PluginFrameworkShutdownEvent(final PluginController pluginController, final PluginAccessor pluginAccessor) {
        super(pluginController, pluginAccessor);
    }
}

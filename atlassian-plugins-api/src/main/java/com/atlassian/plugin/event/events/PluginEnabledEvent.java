package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.Plugin;

/**
 * Event fired when a plugin is enabled, installed, or updated.
 *
 * @see com.atlassian.plugin.event.events
 * @since 2.1.0
 */
@PublicApi
public class PluginEnabledEvent extends PluginEvent {
    public PluginEnabledEvent(final Plugin plugin) {
        super(plugin);
    }
}

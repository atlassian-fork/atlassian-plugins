package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;

/**
 * Event send at the start of a {@link com.atlassian.plugin.PluginController} action wrapping existing plugin events.
 *
 * @see com.atlassian.plugin.event.events
 * @since 5.1.3
 */
@PublicApi
public class PluginTransactionStartEvent {

    private final long threadId;

    public PluginTransactionStartEvent() {
        this.threadId = Thread.currentThread().getId();
    }

    /**
     * @return thread ID of the thread sending this event; can be used to match {@link PluginTransactionStartEvent} and {@link PluginTransactionEndEvent}
     */
    public long threadId() {
        return threadId;
    }
}

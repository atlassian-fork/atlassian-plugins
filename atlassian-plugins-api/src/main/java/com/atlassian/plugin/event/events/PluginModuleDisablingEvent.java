package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.ModuleDescriptor;

/**
 * Event fired when a plugin module is about to be disabled, which can also happen when its
 * plugin is about to be disabled or uninstalled.
 *
 * @see com.atlassian.plugin.event.events
 * @since 3.0.5
 */
@PublicApi
public class PluginModuleDisablingEvent extends PluginModulePersistentEvent {
    public PluginModuleDisablingEvent(final ModuleDescriptor<?> module, final boolean persistent) {
        super(module, persistent);
    }
}

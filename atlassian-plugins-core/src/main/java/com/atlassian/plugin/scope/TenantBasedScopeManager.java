package com.atlassian.plugin.scope;

/**
 * Yet to be implemented.
 * <p>
 * This service will be backed up by TCS and will do scope checks for implicitly derived user/tenant
 *
 * @since 4.1
 * @deprecated in 5.0 for removal in 6.0 when {@link ScopeManager} is removed completely
 */
@Deprecated
public class TenantBasedScopeManager implements ScopeManager {
    @Override
    public boolean isScopeActive(String scopeKey) {
        throw new UnsupportedOperationException("Not implemented");
    }
}

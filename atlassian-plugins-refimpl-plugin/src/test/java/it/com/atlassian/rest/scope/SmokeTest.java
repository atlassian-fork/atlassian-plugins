package it.com.atlassian.rest.scope;

import com.atlassian.rest.jersey.client.WebResourceFactory;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class SmokeTest {

    @Test
    public void testExampleFilterAndServletPresent() {
        final WebResource ws = WebResourceFactory.anonymous(
                WebResourceFactory.getUriBuilder()
                        .path("plugins/servlet/example")
                        .build());

        ClientResponse response = ws.get(ClientResponse.class);

        assertTrue(response.getHeaders().containsKey("example-filtered"));
        assertThat(response.getEntity(String.class), containsString("example-servlet"));
    }
}
